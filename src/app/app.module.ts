import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MySessionsComponent } from './my-sessions/my-sessions.component';
import { MySessionsCalendarComponent } from './my-sessions-calendar/my-sessions-calendar.component';
import { CommitmentsComponent } from './commitments/commitments.component';
import { AllSessionsAttendanceComponent } from './all-sessions-attendance/all-sessions-attendance.component';
import { AllMembersAttendanceComponent } from './all-members-attendance/all-members-attendance.component';
import { NewSessionComponent } from './new-session/new-session.component';
import { ProfileComponent } from './profile/profile.component';
import { SignOutComponent } from './sign-out/sign-out.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {AuthGuard} from './auth.guard';
import { HttpModule } from '@angular/http';
import {AuthService} from './auth.service';
import {MemberProfileService} from './member-profile.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CalendarComponent } from './calendar/calendar.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MySessionsComponent,
    MySessionsCalendarComponent,
    CommitmentsComponent,
    AllSessionsAttendanceComponent,
    AllMembersAttendanceComponent,
    NewSessionComponent,
    ProfileComponent,
    SignOutComponent,
    LoginComponent,
    SignUpComponent,
    PageNotFoundComponent,
    CalendarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    MemberProfileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
