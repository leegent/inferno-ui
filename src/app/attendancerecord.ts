import {MemberProfile} from './memberprofile';

export class AttendanceRecord {
    url: string;
    member: MemberProfile; // a URL when ARs are queried in isolation, beware!
    session: string; // a URL
    arrived_late: boolean;
    left_early: boolean;
}
