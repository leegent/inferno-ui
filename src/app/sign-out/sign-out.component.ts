import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.css']
})
export class SignOutComponent implements OnInit {

  constructor(private _router: Router, private _auth: AuthService) {
    this._auth.logout();
    this._router.navigate(['login']);
  }

  ngOnInit() {
  }

}
