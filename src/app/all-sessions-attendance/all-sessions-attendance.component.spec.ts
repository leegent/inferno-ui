import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllSessionsAttendanceComponent } from './all-sessions-attendance.component';

describe('AllSessionsAttendanceComponent', () => {
  let component: AllSessionsAttendanceComponent;
  let fixture: ComponentFixture<AllSessionsAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllSessionsAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllSessionsAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
