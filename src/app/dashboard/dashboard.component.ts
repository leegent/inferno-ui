import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public calendarURL: string;
  constructor(private _auth: AuthService) { }

  ngOnInit() {
        this.calendarURL = `${environment.host}${environment.allEventsCalendarURL}`;

  }

}
