import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private leagueName: string;

  constructor(private _router: Router, private _auth: AuthService) {
    this.leagueName = environment.leagueName;
  }

  ngOnInit() {
  }

  login(event, username, password) {
    event.preventDefault();
    this._auth.login(username, password);
  }

  signup(event) {
    event.preventDefault();
    this._router.navigate(['signup']);
  }

}
