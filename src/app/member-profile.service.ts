import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MemberProfile} from './memberprofile';
import {Http, Response, Headers} from '@angular/http';
import {environment} from '../environments/environment';
import 'rxjs/add/operator/map';


@Injectable()
export class MemberProfileService {

  constructor(private _http: Http) { }

  getMemberProfileForToken(token: string): Observable<MemberProfile> {
    const url = `${environment.host}${environment.getProfileUrl}`;

    const headers: Headers = new Headers({
      'Authorization': `Token ${token}`
    });

    return this._http.get(url, { headers: headers }).map(
      (data: Response) => {
        return data.json() as MemberProfile;
      },
      error => {
        alert(error.text());
        console.log(error.text());
      }
    );
  }
}
