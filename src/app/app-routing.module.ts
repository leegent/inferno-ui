import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MySessionsComponent} from './my-sessions/my-sessions.component';
import {MySessionsCalendarComponent} from './my-sessions-calendar/my-sessions-calendar.component';
import {CommitmentsComponent} from './commitments/commitments.component';
import {AllSessionsAttendanceComponent} from './all-sessions-attendance/all-sessions-attendance.component';
import {AllMembersAttendanceComponent} from './all-members-attendance/all-members-attendance.component';
import {NewSessionComponent} from './new-session/new-session.component';
import {ProfileComponent} from './profile/profile.component';
import {SignOutComponent} from './sign-out/sign-out.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {AuthGuard} from './auth.guard';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'my-sessions',
    component: MySessionsComponent
  },
  {
    path: 'my-sessions-calendar',
    component: MySessionsCalendarComponent
  },
  {
    path: 'commitments',
    component: CommitmentsComponent
  },
  {
    path: 'sessions-attendance',
    component: AllSessionsAttendanceComponent
  },
  {
    path: 'members-attendance',
    component: AllMembersAttendanceComponent
  },
  {
    path: 'new-session',
    component: NewSessionComponent
  },
  {
    path: 'my-profile',
    component: ProfileComponent
  },
  {
    path: 'sign-out',
    component: SignOutComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
