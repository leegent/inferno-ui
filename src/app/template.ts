import { Tier } from './tier';

export class Template {
    url: string;
    name: string;
    default_leader: string; // a URL
    default_place: string;
    required_tiers: Tier[];
    eligible_tiers: Tier[];
}
