import { Template } from './template';
import {MemberProfile} from './memberprofile';
import {AttendanceRecord} from './attendancerecord';

export class Session {
    url: string;
    id: number;
    template: Template;
    leader: MemberProfile;
    time: string;
    place: string;
    attendancerecord_set: AttendanceRecord[];
}
