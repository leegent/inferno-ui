import { Tier } from './tier';
import { Moment } from 'moment/moment';

export class TierMembership {
  url: string;
  id: number;
  member: string;
  tier: Tier;
  joined: Moment;
  left: Moment;
}
