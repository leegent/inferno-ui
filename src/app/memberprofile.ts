import { User } from './user';
import { TierMembership } from './tier-membership';

export class MemberProfile {
    url: string;
    id: number;
    user: User;
    tiermembership_set: TierMembership[];
}
