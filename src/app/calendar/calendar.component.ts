import {Component, Input, OnInit, AfterViewInit} from '@angular/core';
import * as $ from 'jquery';
import 'fullcalendar';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-calendar',
  template: '<div></div>',
})
export class CalendarComponent implements OnInit, AfterViewInit {
  @Input() calendarUrl: string;

  constructor(private auth: AuthService) {

  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('app-calendar').fullCalendar({
        eventSources: [
          {
            url: this.calendarUrl,
            headers: {
              Authorization: `Token ${this.auth.token()}`
            }
          }]
      });
    }, 100);
  }
}
