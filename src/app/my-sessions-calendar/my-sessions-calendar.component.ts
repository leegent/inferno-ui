import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-my-sessions-calendar',
  templateUrl: './my-sessions-calendar.component.html',
  styleUrls: ['./my-sessions-calendar.component.css']
})
export class MySessionsCalendarComponent implements OnInit {
  public calendarURL: string;

  constructor(private auth: AuthService) {
    this.calendarURL = `${environment.host}${environment.myCalendarUrl}`;

  }

  ngOnInit() {

  }


}
