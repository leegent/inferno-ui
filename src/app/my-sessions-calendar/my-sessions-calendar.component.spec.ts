import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MySessionsCalendarComponent } from './my-sessions-calendar.component';

describe('MySessionsCalendarComponent', () => {
  let component: MySessionsCalendarComponent;
  let fixture: ComponentFixture<MySessionsCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MySessionsCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MySessionsCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
