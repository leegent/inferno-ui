import { Component, OnInit } from '@angular/core';
import {AuthService} from './auth.service';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  leagueName: string;
  icalUrl = 'http://inferno.roller-derby.rocks/calendar.ics/';
  adminUrl = 'http://inferno.roller-derby.rocks/admin';

  constructor(private _auth: AuthService) {
    this.leagueName = environment.leagueName;
  }

  ngOnInit() {
  }

}
