import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMembersAttendanceComponent } from './all-members-attendance.component';

describe('AllMembersAttendanceComponent', () => {
  let component: AllMembersAttendanceComponent;
  let fixture: ComponentFixture<AllMembersAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMembersAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMembersAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
