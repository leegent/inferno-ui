import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Session} from './session';

@Injectable()
export class SessionService {
    private url = 'https://harlots-inferno.herokuapp.com/api/v1/sessions/';

    constructor(private http: Http) { }

    getSessions(): Promise<Session[]> {
        return this.http.get(this.url)
            .toPromise()
            .then(response => response.json() as Session[])
            .catch(this.handleError)

    }
    private handleError(error: any): Promise<any> {
        console.error('An error has occurred', error); // for demo only!
        return Promise.reject(error.message || error);
    }
}
