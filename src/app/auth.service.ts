import { Injectable } from '@angular/core';
import {Http, Headers, Response } from '@angular/http';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';
import {MemberProfileService} from './member-profile.service';
import {MemberProfile} from './memberprofile';

@Injectable()
export class AuthService {

  constructor(private _router: Router,
              private _http: Http,
              private _memberProfileService: MemberProfileService) {

  }

  login(username, password) {
    localStorage.setItem('username', username);
    const url = `${environment.host}${environment.loginUrl}`;

    const body = JSON.stringify({username, password});
    const headers: Headers = new Headers({
      'Content-Type': 'application/json'
    });
    this._http.post(url, body, { headers: headers })
      .subscribe(
        (data: Response) => {
          const token: string = data.json().token;
          localStorage.setItem('id_token', token);

          // Now retrieve and stash the Profile object:
          this._memberProfileService.getMemberProfileForToken(token).subscribe((profile: MemberProfile) => {
              localStorage.setItem('profile', JSON.stringify(profile));
            },
            error => {
              alert(error.text());
              console.log(error.text());
            });

          this._router.navigate(['dashboard']);
        },
        error => {
          alert(error.text());
          console.log(error.text());
        }
      );
  }

  logout() {
    localStorage.removeItem('username');
    localStorage.removeItem('profile');
    localStorage.removeItem('id_token');
  }

  isLoggedIn() {
    const token: string = localStorage.getItem('id_token');
    return (token && (token !== ''));
  }

  token(): string {
    return localStorage.getItem('id_token');
  }

  profile(): MemberProfile {
    const j = localStorage.getItem('profile');
    const o = JSON.parse(j);
    return o as MemberProfile;
  }

  displayName(): string {
    const j = localStorage.getItem('profile');
    if (j !== undefined) {
          const o = JSON.parse(j) as MemberProfile;
          return o.user.name;
    }
    const i = localStorage.getItem('username');
    return i;
  }

}
