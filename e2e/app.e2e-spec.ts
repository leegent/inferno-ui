import { InfernoUiPage } from './app.po';

describe('inferno-ui App', () => {
  let page: InfernoUiPage;

  beforeEach(() => {
    page = new InfernoUiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
